//Webex Bot Starter - featuring the webex-node-bot-framework - https://www.npmjs.com/package/webex-node-bot-framework

var framework = require('webex-node-bot-framework');
var bot = require('webex-node-bot-framework/lib/bot');
var webhook = require('webex-node-bot-framework/webhook');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
app.use(express.static('images'));
const config = require("./config.json");


// init framework
var framework = new framework(config);
framework.start();

console.log("Starting framework, please wait...");

framework.on("initialized", () => {
  console.log("framework is all fired up! [Press CTRL-C to quit]");
});

// A spawn event is generated when the framework finds a space with your bot in it
// If actorId is set, it means that user has just added your bot to a new space
// If not, the framework has discovered your bot in an existing space
framework.on('spawn', (bot, id, actorId) => {

  if (!actorId) {
    // don't say anything here or your bot's spaces will get
    // spammed every time your server is restarted

    console.log(`While starting up, the framework found our bot in a space called: ${bot.room.title}`);
  } else {
    // When actorId is present it means someone added your bot got added to a new space
    // Lets find out more about them..
    var msg = 'Welcome!!';
    bot.webex.people.get(actorId).then((user) => {
      msg = `Hello there ${user.displayName}. ${msg}`; 
    }).catch((e) => {
      console.error(`Failed to lookup user details in framwork.on("spawn"): ${e.message}`);
      msg = `Hello there. ${msg}`;  
    }).finally(() => {
      // Say hello, and tell users what you do!
      if (bot.isDirect) {
        bot.say('markdown', msg);
      } else {
        let botName = bot.person.displayName;
        msg += `\n\nDon't forget, in order for me to see your messages in this group space, be sure to *@mention* ${botName}.`;
        bot.say('markdown', msg);
      }
    });
  }
});


//Process incoming messages

let responded = false;

framework.hears('createroom', function (bot, trigger) {
  framework.debug(`create room: ${trigger.text} requested`);
  console.log(`Bot email ${bot.email}`);
  responded = true;

  var commandArgs = trigger.text;
  var args = commandArgs.split(" ");
  var roomName = args[0];
  var emails = args.slice(1).enqueue(bot.email);

  console.log(`roomName ${roomName}`);
  console.log(`emails ${emails}`);
  console.log(`Bot email length ${emails.length}`);

  bot.newRoom(roomName, emails, false).catch((e) => console.error(e));
});

framework.hears('hello', function(bot, trigger) {
  framework.debug(`hello requested`);
  bot.say('Hello %s!', trigger.person.displayName);
  responded = true;
});


//Server config & housekeeping
// Health Check
app.get('/', function (req, res) {
  res.send(`I'm alive.`);
});

app.post('/', webhook(framework));

var server = app.listen(config.port, function () {
  framework.debug('framework listening on port %s', config.port);
});

// gracefully shutdown (ctrl-c)
process.on('SIGINT', function () {
  framework.debug('stoppping...');
  server.close();
  framework.stop().then(function () {
    process.exit();
  });
});
